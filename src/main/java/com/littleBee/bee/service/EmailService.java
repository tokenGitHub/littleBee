package com.littleBee.bee.service;

public interface EmailService {
    String sendSimpleMail(String toAddress) throws Exception;
}
